CVE-2019-20446
	[buster] - librsvg 2.44.10-2.1+deb10u1
CVE-2019-14267
	[buster] - pdfresurrect 0.15-2+deb10u1
CVE-2019-1020014
	[buster] - golang-github-docker-docker-credential-helpers 0.6.1-2+deb10u1
CVE-2019-17134
	[buster] - octavia 3.0.0-3+deb10u1
CVE-2019-14433
	[buster] - nova 2:18.1.0-6+deb10u1
CVE-2019-14857
	[buster] - libapache2-mod-auth-openidc 2.3.10.2-1+deb10u1
CVE-2021-20217
	[buster] - privoxy 3.0.28-2+deb10u1
CVE-2021-20216
	[buster] - privoxy 3.0.28-2+deb10u1
CVE-2020-35502
	[buster] - privoxy 3.0.28-2+deb10u1
CVE-2021-20209
	[buster] - privoxy 3.0.28-2+deb10u1
CVE-2021-20210
	[buster] - privoxy 3.0.28-2+deb10u1
CVE-2021-20211
	[buster] - privoxy 3.0.28-2+deb10u1
CVE-2021-20212
	[buster] - privoxy 3.0.28-2+deb10u1
CVE-2021-20213
	[buster] - privoxy 3.0.28-2+deb10u1
CVE-2021-20214
	[buster] - privoxy 3.0.28-2+deb10u1
CVE-2021-20215
	[buster] - privoxy 3.0.28-2+deb10u1
